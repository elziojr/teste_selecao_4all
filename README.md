# Teste para seleção de desenvolvedor 4all

Projeto criado para teste de seleção
<br><br>
Caso queira alterar a url da API deve ser alterado o arquivo .env, key **REACT_APP_API_URL**
<br>
Caso queira usar a sua key do maps altere no arquivo .env a key **REACT_APP_MAPS_KEY**
<br><br>
Para iniciar execute os seguintes comandos no diretório do projeto:
<br>
- `npm install` || `yarn install`
- `npm start` || `yarn install`