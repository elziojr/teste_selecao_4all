import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import { Warning } from 'material-ui-icons'

import Styles from './RequestTarefasFailed.styles'

class RequestTarefasFailed extends Component {
  render () {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.centerDiv}>
          <Warning />
          <br />
          Ocorreu algum erro ao obter as tarefas
        </div>
      </div>
    )
  }
}

export default withStyles(Styles)(RequestTarefasFailed)
