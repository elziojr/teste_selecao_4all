import { call, put } from 'redux-saga/effects'

import * as HomeActions from './Home.actions'
import * as HomeAPI from './Home.api'

export function * getTarefasSaga () {
  try {
    yield put(HomeActions.isFetching(true))
    const { data } = yield call(HomeAPI.getTarefas)
    yield put(HomeActions.getTarefasSucceeded(data))
  } catch (e) {
    yield put(HomeActions.getTarefasFailed(e.message))
  }
}
