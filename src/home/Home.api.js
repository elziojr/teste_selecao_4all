import axios from 'axios'

const urlTarefas = '/tarefa/'

export const getTarefas = () => {
  return axios.get(urlTarefas)
}
