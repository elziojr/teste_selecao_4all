import * as HomeActions from './Home.actions'

export default (state = {
  tarefas: [],
  fetching: false
}, action) => {
  switch (action.type) {
    case HomeActions.GET_TAREFAS_SUCCEEDED:
      return {
        ...state,
        fetching: false,
        tarefas: action.payload.tarefas.lista
      }

    case HomeActions.IS_FETCHING:
      return {
        ...state,
        // Seta o isFetching apenas na primeira requisição
        // os outros refresh que são feitos em background n precisa setar isso
        fetching: state.tarefas.length ? state.fetching : action.payload.fetching
      }

    default:
      return state
  }
}
