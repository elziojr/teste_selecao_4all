import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import { CheckCircle } from 'material-ui-icons'

import Styles from './EmptyTarefas.styles'

class EmptyTarefas extends Component {
  render () {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.centerDiv}>
          <CheckCircle />
          <br />
          Nenhuma nova tarefa encontrada
        </div>
      </div>
    )
  }
}

export default withStyles(Styles)(EmptyTarefas)
