export default theme => ({
  root: {
    width: '100%',
    textAlign: 'center',
    flex: 1,
    height: '100%'
  },

  centerDiv: {
    position: 'relative',
    top: '45%',
    transform: 'translateY(-45%)'
  }
})
