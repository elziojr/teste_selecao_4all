import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { withStyles } from 'material-ui/styles'

import { getTarefasSaga } from './Home.actions'

import DefaultLayout from '../layouts/DefaultLayout.component'
import Loading from '../shared/Loading.component'
import EmptyTarefas from './emptyTarefas/EmptyTarefas.component'
import ItemListTarefa from '../tarefa/ItemListTarefa.component'

import Styles from './Home.styles'

class Home extends Component {
  componentWillMount () {
    this.props.getTarefasSaga()

    // Depois de 3 segundos obtem de novo a lista de tarefas, pra manter sempre atualizado
    setInterval(() => {
      if (this.props.fetching) {
        return
      }

      this.props.getTarefasSaga()
    }, 3000)
  }

  render () {
    const { classes } = this.props

    return (
      <DefaultLayout innerComponent={
        <span>Tarefas</span>
      }>{
        // Obtendo as tarefas ?
        (this.props.fetching)
        ? <Loading />

        // Caso !length -> Nenhuma tarefa a ser exibida
        : (!this.props.fetching && !this.props.tarefas.length)
        ? <EmptyTarefas />

        // Caso não esteja obtendo, sem erros, e tenha length
        : <div className={classes.root}>
          { this.props.tarefas.map((tarefa, key) => {
            return <ItemListTarefa key={key} tarefa={tarefa} />
          }) }
        </div>
      }</DefaultLayout>
    )
  }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired
}

const dispatchToProps = dispatch => bindActionCreators({
  getTarefasSaga: getTarefasSaga
}, dispatch)

const mapStateToProps = state => ({
  tarefas: state.home.tarefas,
  fetching: state.home.fetching
})

export default connect(mapStateToProps, dispatchToProps)(withStyles(Styles)(Home))
