export const IS_FETCHING = 'IS_FETCHING'
export const GET_TAREFAS_SAGA = 'GET_TAREFAS_SAGA'
export const GET_TAREFAS_SUCCEEDED = 'GET_TAREFAS_SUCCEEDED'
export const GET_TAREFAS_FAILED = 'GET_TAREFAS_FAILED'

export const isFetching = fetching => {
  return {
    type: IS_FETCHING,
    payload: { fetching }
  }
}

export const getTarefasSaga = () => {
  return {
    type: GET_TAREFAS_SAGA
  }
}

export const getTarefasSucceeded = tarefas => {
  return {
    type: GET_TAREFAS_SUCCEEDED,
    payload: { tarefas }
  }
}

export const getTarefasFailed = message => {
  return {
    type: GET_TAREFAS_FAILED,
    payload: { message }
  }
}
