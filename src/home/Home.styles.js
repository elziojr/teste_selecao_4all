export default theme => ({
  root: {
    textAlign: 'center',
    flex: 1,
    width: '100%',
    paddingTop: '4px'
  }
})
