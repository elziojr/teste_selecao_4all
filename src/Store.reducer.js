import { combineReducers } from 'redux'

import home from './home/Home.reducer'
import tarefa from './tarefa/Tarefa.reducer'

const allReducers = combineReducers({
  home,
  tarefa
})

export default allReducers
