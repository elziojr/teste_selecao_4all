import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router-dom'

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import { amber, grey, red } from 'material-ui/colors'

import './App.styles.css'
import 'typeface-roboto'

import Home from './home/Home.component'
import Tarefa from './tarefa/Tarefa.component'
import Servicos from './tarefa/servicos/Servicos.component'

class App extends Component {
  constructor () {
    super()
    this.theme = createMuiTheme({
      palette: {
        primary: {
          ...amber,
          500: 'rgb(214, 143, 0)'
        },
        secondary: {
          ...grey,
          A100: 'rgb(255,255,255)',
          A200: 'rgb(255,255,255)',
          A400: 'rgb(255,255,255)',
          A700: 'rgb(255,255,255)'
        },
        error: red
      }
    })
  }

  render () {
    const { Router, store } = this.props

    return (
      <MuiThemeProvider theme={this.theme}>
        <Provider store={store}>
          <Router>
            <Switch>
              <Route exact path='/'>
                <Redirect from='/' to='tarefa' />
              </Route>
              <Route exact path='/tarefa' component={Home} />
              <Route exact path='/tarefa/:idTarefa' component={Tarefa} />
              <Route exact path='/tarefa/:idTarefa/servicos' component={Servicos} />
            </Switch>
          </Router>
        </Provider>
      </MuiThemeProvider>
    )
  }
}

export default App
