import { takeEvery } from 'redux-saga/effects'

import * as HomeActions from './home/Home.actions'
import * as HomeSagas from './home/Home.saga'
import * as TarefaActions from './tarefa/Tarefa.actions'
import * as TarefaSagas from './tarefa/Tarefa.saga'

// Declaração de todas as sagas devem ser feitas aqui
function * Sagas () {
  yield takeEvery(HomeActions.GET_TAREFAS_SAGA, HomeSagas.getTarefasSaga)
  yield takeEvery(TarefaActions.GET_DETALHES_TAREFA_SAGA, TarefaSagas.getTarefaSaga)
}

export default Sagas
