import React, { Component } from 'react'

import { withRouter } from 'react-router-dom'

import DefaultLayout from '../../layouts/DefaultLayout.component'

class Servicos extends Component {
  render () {
    return (
      <DefaultLayout innerComponent={
        <span>Serviços</span>
      }>{
        <span>Serviços</span>
      }</DefaultLayout>
    )
  }
}

export default withRouter(Servicos)
