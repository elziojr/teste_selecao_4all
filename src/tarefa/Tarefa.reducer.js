import * as TarefaActions from './Tarefa.actions'

export default (state = {
  fetching: true
}, action) => {
  switch (action.type) {
    case TarefaActions.GET_DETALHES_TAREFA_SAGA_SUCCEEDED:
      return {
        ...state,
        fetching: false,
        tarefa: action.payload.tarefa
      }

    case TarefaActions.IS_DETALHES_TAREFA_FETCHING:
      return {
        ...state,
        fetching: state.tarefa ? state.fetching : action.payload.fetching
      }

    case TarefaActions.CLEAR_TAREFA:
      state.fetching = true
      delete state[ 'tarefa' ]

      return {
        ...state
      }

    default:
      return state
  }
}
