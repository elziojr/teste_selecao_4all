const height = window.screen.height
const largeScreen = height > 700

export default theme => ({
  root: {
    width: '100%',
    height: '100%',
    textAlign: 'left'
  },

  // ---------- BAR ----------
  backButtonGrid: {
    paddingLeft: '0 !important',
    textAlign: 'left'
  },

  searchButtonGrid: {
    textAlign: 'right'
  },

  barTitle: {
    fontSize: largeScreen ? '21px' : '14px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },

  // ---------- BACKGROUND IMAGE ----------
  divPicture: {
    width: '100%',
    overflow: 'hidden',
    height: height / 3,
    padding: '0 !important'
  },

  backgroundPicture: {
    width: '100%',
    minHeight: '100%',
    maxHeight: '180%'
  },

  // ---------- TITLE & LOGO ----------
  containerLogo: {
    width: '100%',
    minHeight: '10%',
    margin: 0
  },

  logoTitle: {
    fontSize: '25px',
    paddingLeft: '4% !important',
    verticalAlign: 'middle',
    height: (largeScreen ? height / 8 : height / 18) + 'px',
    lineHeight: (largeScreen ? height / 8 : height / 18) + 'px',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    color: theme.palette.primary[500]
  },

  logoAvatar: {
    height: largeScreen ? height / 10 : height / 18
  },

  logo: {
    top: largeScreen ? -height / 10 : -height / 18,
    width: largeScreen ? height / 5 : height / 9,
    height: largeScreen ? height / 5 : height / 9,
    marginLeft: 'calc(90% - ' + (largeScreen ? height / 5 : height / 9) + 'px)',
    backgroundColor: 'white'
  },

  // ---------- DATA PANEL ----------
  panel: {
    width: '100%',
    height: height / 1.5,
    margin: 0,
    backgroundColor: '#efefef'
  },

  actions: {
    margin: 0,
    marginTop: 10,
    textAlign: 'center',
    verticalAlign: 'middle',
    lineHeight: largeScreen ? '100px' : '50px',
    backgroundColor: 'white',
    cursor: 'pointer',
    color: theme.palette.primary[500]
  },

  imageActions: {
    width: largeScreen ? '30%' : '60%',
    maxHeight: '100%'
  },

  actionItem: {
    lineHeight: 0,
    fontSize: largeScreen ? '16px' : '10px',
    overflow: 'hidden'
  },

  divider: {
    width: '90%',
    margin: '1%',
    marginLeft: '5%'
  },

  text: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    fontSize: largeScreen ? '18px' : '14px',
    width: '100%',
    padding: '5%',
    paddingTop: '2%',
    paddingBottom: '2%',
    backgroundColor: 'white',
    color: theme.palette.primary[500]
  },

  // ---------- MAP ----------
  mapContainer: {
    position: 'relative',
    height: height > 700 ? '40%' : '20%',
    width: '100%'
  },

  // ---------- COMENTARY ----------
  comentaryContainer: {
    marginTop: '2%',
    width: '100%'
  }
})
