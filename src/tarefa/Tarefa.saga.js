import { call, put } from 'redux-saga/effects'

import * as TarefaActions from './Tarefa.actions'
import * as TarefaAPI from './Tarefa.api'

export function * getTarefaSaga (action) {
  try {
    yield put(TarefaActions.isFetching(true))
    const { data } = yield call(() => TarefaAPI.getTarefa(action.payload.idTarefa))
    yield put(TarefaActions.getTarefaSagaSucceeded(data))
  } catch (e) {
    console.log(e)
  }
}
