const width = window.screen.width
const largeScreen = width > 700

export default theme => ({
  root: {
    textAlign: 'left',
    width: '100%',
    height: largeScreen ? 100 : 80,
    backgroundColor: 'white',
    color: theme.palette.primary[500],
    fontSize: 12
  },

  comentary: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    paddingTop: '0 !important',
    height: '50%',
    marginTop: largeScreen ? 0 : -8
  },

  avatar: {
    height: 'auto',
    width: 'auto',
    marginTop: '20%',
    maxHeight: '60%',
    maxWidth: largeScreen ? '50%' : '60%',
    borderRadius: '50%'
  },

  avatarContainer: {
    padding: '0 !important'
  },

  rating: {
    textAlign: 'right'
  },

  star: {
    width: largeScreen ? 24 : 12,
    height: largeScreen ? 24 : 12
  },

  container: {
    width: '100%',
    height: '100%',
    padding: '5%',
    margin: 0,
    paddingTop: '1%',
    paddingBottom: '1%'
  },

  dataContainer: {
    padding: '0 !important'
  }
})
