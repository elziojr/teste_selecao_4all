import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { withStyles } from 'material-ui/styles'
import { Star } from 'material-ui-icons'
import { Grid } from 'material-ui'

import Styles from './Comentario.styles'

class Comentario extends Component {
  render () {
    const { classes, comentario } = this.props
    return (
      <div className={classes.root}>
        <Grid container className={classes.container}>
          <Grid item md={1} sm={2} xs={3} container className={classes.avatarContainer}>
            <img alt='avatar' src={comentario.urlFoto} className={classes.avatar} />
          </Grid>
          <Grid item xs container className={classes.dataContainer}>
            <Grid item xs={8}>
              { comentario.nome }
              <br />
              <b>{ comentario.titulo }</b>
            </Grid>
            <Grid item xs={4} className={classes.rating}>{
                [...Array(comentario.nota)].map((value, key) =>
                  <Star key={key} className={classes.star} />
                )
            }</Grid>
            <Grid item xs={12} className={classes.comentary}>
              { comentario.comentario }
            </Grid>
          </Grid>
        </Grid>
      </div>
    )
  }
}

Comentario.propTypes = {
  comentario: PropTypes.object.isRequired
}

export default withStyles(Styles)(Comentario)
