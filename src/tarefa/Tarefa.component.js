import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router-dom'

import { withStyles } from 'material-ui/styles'
import { Search, Reply, LocationOn } from 'material-ui-icons'
import {
    Avatar,
    IconButton,
    Grid,
    Divider,
    Snackbar
} from 'material-ui'

import Loading from '../shared/Loading.component'
import DefaultLayout from '../layouts/DefaultLayout.component'
import MapView from '../shared/MapView.component'
import Comentario from './comentario/Comentario.component'
import Assets from './assets/Assets'

import Styles from './Tarefa.styles'
import { getTarefaSaga, clearTarefa } from './Tarefa.actions'

class Tarefa extends Component {
  constructor () {
    super()
    this.state = {
      snackOpened: false
    }
  }

  componentWillMount () {
    this.props.getTarefaSaga(this.props.match.params.idTarefa)
  }

  componentWillUnmount () {
    this.props.clearTarefa()
  }

  handleClickAction (actionName) {
    switch (actionName) {
      case 'ligar':
        window.open('tel:+' + this.props.tarefa.telefone)
        break
      case 'servicos':
        this.props.history.push('/tarefa/' + this.props.tarefa.id + '/servicos')
        break
      case 'endereco':
        this.setState({ snackOpened: true })
        break
      case 'comentarios':
        window.scrollTo(0, this.divComentary.offsetTop)
        break
      case 'snackClosed':
        this.setState({ snackOpened: false })
        break
      default:
    }
  }

  render () {
    const {
      classes,
      tarefa,
      history,
      fetching
    } = this.props

    return (
      <DefaultLayout innerComponent={
        // ------- Bar -------
        <Grid container>
          <Grid item xs={2} className={classes.backButtonGrid}>
            <IconButton onClick={history.goBack} color='accent' aria-label='Voltar'>
              <Reply />
            </IconButton>
          </Grid>

          <Grid item xs={8} className={classes.barTitle}>
            <LocationOn />
            { tarefa ? tarefa.cidade + ' - ' + tarefa.bairro : '' }
          </Grid>

          <Grid item xs={2} className={classes.searchButtonGrid}>
            <IconButton color='accent' aria-label='Nao-implementado'>
              <Search />
            </IconButton>
          </Grid>
        </Grid>
      }>{
        // ------- Loading -------
        (fetching)
        ? <Loading />

        // ------- Data panel -------
        : <div className={classes.root}>
          <Grid container className={classes.panel}>
            <Snackbar
              open={this.state.snackOpened}
              onClose={() => this.handleClickAction('snackClosed')}
              autoHideDuration={3000}
              message={tarefa.endereco}
            />

            <Grid item xs={12} className={classes.divPicture}>
              <img alt='background' src={tarefa.urlFoto} className={classes.backgroundPicture} />
            </Grid>

            <Grid container className={classes.containerLogo}>
              <Grid item xs={8} className={classes.logoTitle}>
                <b>{ tarefa.titulo.toUpperCase() }</b>
              </Grid>

              <Grid item xs={4} className={classes.logoAvatar}>
                <Avatar alt='logo' src={tarefa.urlLogo} className={classes.logo} />
              </Grid>
            </Grid>

            <Grid container className={classes.actions}>
              { // ------- Actions -------
                Assets.map((asset, key) => {
                  return (
                    <Grid item xs
                      key={key}
                      className={classes.actionItem}
                      onClick={() => this.handleClickAction(asset.name)}>
                      <img alt={asset.name} className={classes.imageActions} src={asset.image} />
                      <br />
                      <span color='primary'>{ asset.title }</span>
                    </Grid>
                  )
                })}
              <Divider className={classes.divider} />
            </Grid>

            <div className={classes.text} color='primary'>
              { tarefa.texto }
            </div>

            <div className={classes.mapContainer}>
              <MapView lat={tarefa.latitude} lng={tarefa.longitude} />
            </div>

            <div className={classes.comentaryContainer}
              ref={div => { this.divComentary = div }}>{
              tarefa.comentarios.map((comentario, key) => {
                return (
                  <div key={key}>
                    <Comentario comentario={comentario} />
                    <Divider className={classes.divider} />
                  </div>
                )
              })
            }</div>

          </Grid>
        </div>
      }</DefaultLayout>
    )
  }
}

Tarefa.propTypes = {
  classes: PropTypes.object.isRequired
}

const dispatchToProps = dispatch => bindActionCreators({
  getTarefaSaga: getTarefaSaga,
  clearTarefa: clearTarefa
}, dispatch)

const mapStateToProps = state => ({
  tarefa: state.tarefa.tarefa,
  fetching: state.tarefa.fetching
})

export default connect(
  mapStateToProps,
  dispatchToProps
)(withRouter(withStyles(Styles)(Tarefa)))
