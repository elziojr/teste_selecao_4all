export default theme => ({
  root: {
    textAlign: 'left',
    paddingLeft: '16px',
    margin: '8px',
    width: 'auto',
    cursor: 'pointer'
  },

  link: {
    textDecoration: 'none'
  }
})
