import React, { Component } from 'react'

import { Link } from 'react-router-dom'

import { withStyles } from 'material-ui/styles'
import Card, { CardContent } from 'material-ui/Card'
import Typography from 'material-ui/Typography'

import Styles from './ItemListTarefa.styles'

class ItemListTarefa extends Component {
  render () {
    const { classes } = this.props
    return (
      <Link to={'/tarefa/' + this.props.tarefa} className={classes.link}>
        <Card className={classes.root}>
          <CardContent className={classes.content}>
            <Typography type='headline'>Tarefa { this.props.tarefa }</Typography>
            <Typography type='subheading' color='secondary'>
              Clique para acessar
            </Typography>
          </CardContent>
        </Card>
      </Link>
    )
  }
}

export default withStyles(Styles)(ItemListTarefa)
