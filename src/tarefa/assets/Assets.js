import comentarios from './COMENTARIOS.png'
import endereco from './ENDERECO.png'
import favoritos from './FAVORITOS.png'
import ligar from './LIGAR.png'
import servicos from './SERVICOS.png'

export default [
  { image: ligar, name: 'ligar', title: 'Ligar' },
  { image: servicos, name: 'servicos', title: 'Serviços' },
  { image: endereco, name: 'endereco', title: 'Endereço' },
  { image: comentarios, name: 'comentarios', title: 'Comentários' },
  { image: favoritos, name: 'favoritos', title: 'Favoritos' }
]
