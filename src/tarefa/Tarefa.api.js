import axios from 'axios'

const urlTarefa = '/tarefa/'

export const getTarefa = idTarefa => {
  return axios.get(urlTarefa + idTarefa)
}
