export const IS_DETALHES_TAREFA_FETCHING = 'IS_DETALHES_TAREFA_FETCHING'
export const GET_DETALHES_TAREFA_SAGA = 'GET_DETALHES_TAREFA_SAGA'
export const GET_DETALHES_TAREFA_SAGA_SUCCEEDED = 'GET_DETALHES_TAREFA_SAGA_SUCCEEDED'
export const CLEAR_TAREFA = 'CLEAR_TAREFA'

export const isFetching = fetching => {
  return {
    type: IS_DETALHES_TAREFA_FETCHING,
    payload: { fetching }
  }
}

export const getTarefaSaga = idTarefa => {
  return {
    type: GET_DETALHES_TAREFA_SAGA,
    payload: { idTarefa }
  }
}

export const getTarefaSagaSucceeded = tarefa => {
  return {
    type: GET_DETALHES_TAREFA_SAGA_SUCCEEDED,
    payload: { tarefa }
  }
}

export const clearTarefa = () => {
  return {
    type: CLEAR_TAREFA
  }
}
