const width = window.screen.width
// Abaixo de 600px o material ui diminui o tamanho da bar pra 54... sim ¬¬
const menuBarHeight = width >= 600 ? '64px' : '54px'

export default theme => ({
  root: {
    height: '100%'
  },

  children: {
    height: 'calc( 100% - ' + menuBarHeight + ' )',
    marginTop: menuBarHeight
  },

  bar: {
    height: menuBarHeight
  }
})
