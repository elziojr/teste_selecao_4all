import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'

import Bar from '../bar/Bar.component'
import Styles from './DefaultLayout.styles'

export class DefaultLayout extends Component {
  render () {
    const { classes, children } = this.props

    return (
      <div className={classes.root}>
        <Bar className={classes.bar} innerComponent={this.props.innerComponent} />
        <div className={classes.children}>
          { children }
        </div>
      </div>
    )
  }
}

export default withStyles(Styles)(DefaultLayout)
