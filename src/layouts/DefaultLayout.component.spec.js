import { shallow } from 'enzyme'
import React from 'react'

import { DefaultLayout } from './DefaultLayout.component'

it('renders the content', () => {
  const wrapper = shallow(<DefaultLayout classes={{ root: 'root' }}>content</DefaultLayout>)
  expect(wrapper).toMatchSnapshot()
})
