import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { withStyles } from 'material-ui/styles'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'

import Styles from './Bar.styles'

class Bar extends Component {
  render () {
    const classes = this.props.classes

    return (
      <div className={classes.root}>
        <AppBar position='static'>
          <Toolbar>
            <Typography type='title' color='accent' className={classes.label}>
              { this.props.innerComponent }
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

Bar.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(Styles)(Bar)
