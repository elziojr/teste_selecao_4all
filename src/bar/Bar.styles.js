export default () => ({
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    position: 'fixed',
    top: 0,
    zIndex: 200
  },

  label: {
    flex: 1,
    textAlign: 'center'
  }
})
