import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import { createStore } from 'redux'

import Store from '../Store.reducer'
import Bar from './Bar.component'

it('renders without crashing', () => {
  // Testes de frontEnd estou aprendendo ainda...
  const div = document.createElement('div')
  const store = createStore(Store)

  ReactDOM.render(
    <Provider store={store}>
      <Bar />
    </Provider>
  , div)
})
