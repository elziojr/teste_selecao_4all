import React, { Component } from 'react'
import { withStyles } from 'material-ui/styles'
import { CircularProgress } from 'material-ui/Progress'

import Styles from './Loading.styles'

class Loading extends Component {
  render () {
    const { classes } = this.props

    return (
      <div className={classes.root}>
        <div className={classes.labelLoading}>
          <CircularProgress />
        </div>
      </div>
    )
  }
}

export default withStyles(Styles)(Loading)
