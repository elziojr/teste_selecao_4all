import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {
  Map,
  Marker,
  GoogleApiWrapper
} from 'google-maps-react'

export class MapView extends Component {
  render () {
    const { lat, lng } = this.props

    return (
      <Map google={this.props.google}
        zoom={14}
        initialCenter={{ lat, lng }}>
        <Marker position={{ lat, lng }} />
      </Map>
    )
  }
}

MapView.propTypes = {
  lat: PropTypes.number.isRequired,
  lng: PropTypes.number.isRequired
}

export default GoogleApiWrapper({
  apiKey: process.env.REACT_APP_MAPS_KEY
})(MapView)
