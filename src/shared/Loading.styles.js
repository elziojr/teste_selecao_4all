export default theme => ({
  root: {
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    color: 'rgba(0, 0, 0, 0.5)',
    textAlign: 'center',
    flex: 1,
    height: '100%'
  },

  labelLoading: {
    position: 'relative',
    top: '45%',
    transform: 'translateY(-45%)'
  }
})
