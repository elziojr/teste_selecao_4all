import React from 'react'
import ReactDOM from 'react-dom'

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import { BrowserRouter } from 'react-router-dom'

import axios from 'axios'
import dotenv from 'dotenv'

import registerServiceWorker from './registerServiceWorker'
import App from './App.component'
import Store from './Store.reducer'
import Sagas from './Sagas.saga'

// Carrega as variaveis de ambiente
dotenv.config()

// Configs do axios
axios.defaults.baseURL = process.env.REACT_APP_API_URL

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  Store,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(Sagas)

ReactDOM.render(
  <App store={store} Router={BrowserRouter} />,
  document.getElementById('root')
)

registerServiceWorker()
